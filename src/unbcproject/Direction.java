package unbcproject;

import java.util.ArrayList;

public class Direction {
    private final int x;
    private final int y;
    private final int height;

    public Direction(int x, int y, int height){

        this.x = x;
        this.y = y;
        this.height = height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHeight() {
        return height;
    }
    private static ArrayList<Direction> allDirections = null;
    public static ArrayList<Direction> getAllDirections(){
        if(allDirections == null){
            allDirections = new ArrayList<>();
            for (int x = -1; x <= 1; x++) {
                for (int y = -1; y <= 1; y++) {
                    for (int z = -1; z <= 1; z++) {
                        if(!(x == 0 && y == 0 && z == 0)){
                            //0,0,0 is not a direction
                            allDirections.add(new Direction(x, y, z));
                        }
                    }
                }
            }
        }
        return allDirections;
    }

    @Override
    public String toString() {
        return "[" +
                "x=" + x +
                ", y=" + y +
                ", height=" + height +
                ']';
    }
}