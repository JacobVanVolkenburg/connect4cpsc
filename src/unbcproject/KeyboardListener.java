package unbcproject;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * This file is part of a solution to
 * CPSC 110 Lab unbcproject Problem 1
 * <p>
 * [Desc]
 *
 * @version 1
 */

public class KeyboardListener implements KeyListener {
    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        PegSelector currentPlayer = GameComponent.getPlayer();
        switch (keyEvent.getKeyChar()) {
            case 'a':
                currentPlayer.moveLeft();
                break;
            case 'd':
                currentPlayer.moveRight();
                break;
            case 'w'://///
                currentPlayer.moveUp();
                break;
            case 's':
                currentPlayer.moveDown();
                break;
            case ' ':// added the space//
                currentPlayer.placeOnPeg();
                break;
            case 'c':// added the space//
                UserInput.clearBoard();
                break;
        }
        GameFrame.frame.repaint();
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
    }
}
