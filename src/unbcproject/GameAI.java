package unbcproject;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This file is part of a solution to
 * CPSC 110 Lab unbcproject Problem 1
 * <p>
 * [Desc]
 *
 * @author Jacob VanVolkenburg
 * Student Number: 230143007
 * @version 1
 */

public class GameAI {
    private static int randPlace() {
        return (int) (Math.random() * 4);
    }

    public static void nextMove(int player) {
        if (GameComponent.whoWon == 0) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    while(true){
                        boolean placedCorrectly = UserInput.addBead(randPlace(), randPlace());
                        if(placedCorrectly){
                            GameFrame.frame.repaint();
                            break;
                        }
                    }
                }
            }, 100);


        }
    }
}