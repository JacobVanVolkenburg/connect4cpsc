package unbcproject;

import java.awt.*;

/**
 * This file is part of a solution to
 * CPSC 110 Lab unbcproject Problem 1
 * <p>
 * [Desc]
 *
 * @author Jacob VanVolkenburg
 * Student Number: 230143007
 * @version 1
 */

public class PegSelector {
    Rectangle rect;
    Peg selected;
    public PegSelector(int x, int y, int width, int height){
        rect = new Rectangle(x, y, width, height);
    }
    public Rectangle getRectangle(){
        return new Rectangle(selected.getRectangle().x - 8, selected.getRectangle().y, 20, 20);
    }
    public void moveRight(){
        this.selected = selected.getRight();
    }
    public void moveLeft(){
        this.selected = selected.getLeft();
    }
    public void moveUp(){
        this.selected = selected.getUp();
    }//
    public void moveDown(){
        this.selected = selected.getDown();
    }

    public void placeOnPeg() {
        boolean success = selected.dropBead();
        if(success) {
            WinCheck.checkWin(GameComponent.getCurrentPlayer());
            GameComponent.switchPlayer();
        }
    }
}
