package unbcproject;

import java.awt.*;

/**
 * This file is part of a solution to
 * CPSC 110 Lab PACKAGE_NAME Problem 1
 * <p>
 * [Desc]
 *
 * @author Jacob VanVolkenburg
 * Student Number: 230143007
 * @version 1
 */

public class ColorPalette {
    public static Color background = new Color(74, 73, 74);
    public static Color player1 = new Color(255, 41, 64);
    public static Color player2 = new Color(255, 243, 0);
    public static Color pegColor = new Color(128, 237, 160);
    public static Color lineColor = new Color(0,0,0);
    public static Color titleColor = new Color(255, 255, 255);
    public static Color winColor = new Color(202, 253, 255);

    /*public static Color background = new Color(0, 0, 0);
    public static Color player1 = new Color(255, 0, 24);
    public static Color player2 = new Color(255, 243, 0);
    public static Color pegColor = new Color(22, 237, 0);
    public static Color lineColor = new Color(255, 255, 255);
    public static Color titleColor = new Color(255, 255, 255);
    public static Color winColor = new Color(255, 0, 248);*/
}
