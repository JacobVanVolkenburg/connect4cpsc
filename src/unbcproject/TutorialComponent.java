package unbcproject;

import javax.swing.*;
import java.awt.*;

/**
 * This file is part of a solution to
 * CPSC 110 Lab unbcproject Problem 1
 * <p>
 * [Desc]
 *
 * @author Jacob VanVolkenburg
 * Student Number: 230143007
 * @version 1
 */

public class TutorialComponent extends JTextArea {
    private static String tutorialContents =
            "How to play: \n" +
            " W/A/S/D to move controller\n"+
                    " Space to place bead\n"+
                    "Debug Commands: \n"+
                    " C to clear\n"+
                    " x to exit\n";
    public TutorialComponent() {
        setText(tutorialContents);
        setEditable(false);
    }

}
