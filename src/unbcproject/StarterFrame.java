package unbcproject;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

/**
 * This file is part of a solution to
 * CPSC 110 Lab unbcproject Problem 1
 * <p>
 * [Desc]
 *
 * @author Jacob VanVolkenburg
 * Student Number: 230143007
 * @version 1
 */

public class StarterFrame {
    public static void main(String[] args) {

        JFrame fFrame = new JFrame();
        JPanel panel  = new JPanel();
        fFrame.setSize(300, 400);
        fFrame.setTitle("Welcome to connect 4");
        fFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        fFrame.add(panel);

        JCheckBox checkBox = new JCheckBox("Select starting colour");
        panel.add(checkBox);
        checkBox.setBackground(ColorPalette.player1);
        checkBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(checkBox.isSelected()) {
                    checkBox.setBackground(ColorPalette.player2);
                }else{
                    checkBox.setBackground(ColorPalette.player1);

                }
            }
        });

        JButton pvpButton = new JButton("Player vs Player");
        pvpButton.addActionListener(actionEvent -> {
            fFrame.dispose();
            GameFrame.startFrame(false, false, checkBox.isSelected());
        });//
        JButton pvcButton = new JButton("Player vs Computer");
        pvcButton.addActionListener(actionEvent -> {
            fFrame.dispose();
                    GameFrame.startFrame(false, true, checkBox.isSelected());
        });



        panel.add(pvpButton);
        panel.add(pvcButton);
        fFrame.setVisible(true);
        panel.add(new TutorialComponent());


        UserInput.showAllInputs();
        Scanner scanner = new Scanner(System.in);
        while(true){
            UserInput.askForNextInput();
            UserInput.getNextInput(scanner);
        }
    }
}
