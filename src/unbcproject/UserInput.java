package unbcproject;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Scanner;

/**
 * This file is part of a solution to
 * CPSC 110 Lab unbcproject Problem 1
 * <p>
 * [Desc]
 *
 * @author Jacob VanVolkenburg
 * Student Number: 230143007
 * @version 1
 */

public class UserInput {

    public static void showAllInputs(){
        System.out.println("help, clear, quit, go, getai");
    }
    public static void askForNextInput(){
        System.out.println("Enter input: ");
    }
    public static void getNextInput(Scanner scanner){
        String s = scanner.nextLine();
        System.out.println("[" + s + "]");
        if(s.equals("clear")){
            clearBoard();
        }else if(s.equals("quit")){
            quit();
        }else if(s.equals("go interactive")){
            goInteractive();
        }else if (s.equals("getai")){
            doAiMove();
        }else if (s.equals("show board")){
            System.out.println("Look at the GUI");
        }else if (s.equals("draw board")){
            System.out.println("Look at the GUI");
        }else if (s.startsWith("addbead")){
            //s = addbead 1 2
            String[] list = s.split(" ");
            if(list.length == 3){
                int x = Integer.parseInt(list[1]);
                int y = Integer.parseInt(list[2]);
                int whoPlaced = GameComponent.getCurrentPlayer();
                addBead(x, y);

                GameFrame.frame.repaint();
            }else{
                System.out.println("Usage: addbead x y");
            }
        }else{
            showAllInputs();
        }
    }
    public static void clearBoard(){
        for (Peg peg : GameComponent.getPegs()){
            peg.clearBeads();
        }
        GameComponent.playerWin(0);
        GameFrame.frame.repaint();
    }
    public static void quit(){
        System.exit(1);
    }
    public static void draw(){

    }
    public static void doAiMove(){


    }
    public static boolean addBead(int x, int y){
        boolean success = GameFrame.pegArray[x][y].dropBead();
        if(success) {
            GameComponent.switchPlayer();
        }
        WinCheck.checkWin(GameComponent.getCurrentPlayer());
        return success;
    }
    public static void removeBead(int x, int y, int r){
        Peg peg = GameFrame.pegArray[x][y];
        peg.removeBeadAt(r);

    }
    public static void goInteractive(){
        GameFrame.frame.setVisible(true);
    }

    public static void resetGame() {
        clearBoard();
    }
}
/*
 clean up member variables
 javadoc comments
 improve AI
 */
