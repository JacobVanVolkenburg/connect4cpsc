package unbcproject;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;

/**
 * This file is part of a solution to
 * CPSC 110 Lab unbcproject Problem 1
 * <p>
 * [Desc]
 *
 * @author Jacob VanVolkenburg
 * Student Number: 230143007
 * @version 1
 */

public class GameComponent extends JComponent {
    static int i = 0;
    private static ArrayList<Peg> pegs = new ArrayList<>();
   /// public static ArrayList<Bead> beads = new ArrayList<>();
    private static PegSelector player1 = new PegSelector(0, 0, 10, 10);
    private static int currentPlayer = 1;


    public static void switchPlayer() {
        if (currentPlayer == 1) {
            System.out.println("1");
            currentPlayer = 2;
            if(GameFrame.player2AI){
                System.out.println("2");
                GameAI.nextMove(2);
            }
        } else {
            System.out.println("3");
            currentPlayer = 1;
            if(GameFrame.player1AI){
                System.out.println("4");
                GameAI.nextMove(1);
            }
        }
    }
    public static int whoWon = 0;
    public static void playerWin(int player){
        whoWon = player;
    }
    /////////////////////////////

    public static void markBeadsAsGreen(ArrayList<Bead> beads){
        for (Bead bead : beads) {
            bead.setColor(ColorPalette.winColor);
        }
    }

    public static PegSelector getPlayer() {
        return player1;
    }

    public static void setCurrentPlayer(int p) {
        currentPlayer = p;
    }

    protected void paintComponent(Graphics g) {
        Graphics2D g2 = ((Graphics2D) g);


        // setting background for the game

        Rectangle backGround = new Rectangle(0, 0, 800, 800);
        g2.draw(backGround);
        g2.setColor(ColorPalette.background);

        g2.fill(backGround);

        // sets color for pegs

        //Draw Outline
        g2.setColor(ColorPalette.lineColor);
        drawBoard(g2);
        for (Peg peg : pegs) {
            g2.setColor(ColorPalette.pegColor);
            g2.fill(peg.getRectangle());
            peg.drawBeads(g2);
        }

        //Draw players
        if (currentPlayer == 1) {
            g2.setColor(ColorPalette.player1);
        } else {
            g2.setColor(ColorPalette.player2);
        }

        g2.fill(player1.getRectangle());

        // write connect 4 on the screen
        g2.setColor(ColorPalette.titleColor);
        g2.setFont(g2.getFont().deriveFont(30F));
        g2.drawString("3D CONNECT 4", 325, 50);

        if(whoWon != 0){
            g2.setFont(g2.getFont().deriveFont(50F));
            if(whoWon == 1){
                g2.setColor(ColorPalette.player1);
                g2.drawString("RED PLAYER HAS WON", 100, 700);
            }else{
                g2.setColor(ColorPalette.player2);
                g2.drawString("YELLOW PLAYER HAS WON", 100, 700);

            }
            g2.setColor(ColorPalette.winColor);
            g2.setFont(g2.getFont().deriveFont(20F));
            g2.drawString("Press 'c' to reset", 100, 740);

        }
        g2.setColor(ColorPalette.background);
        g2.setFont(g2.getFont().deriveFont(20F));
        String[] allStrings = new String[]{"How to play:", "WASD to move", "c to clear", "space to drop"};
        int i = 0;
        for (String line : allStrings){
            i+=30;
            g2.drawString(line, 820, 40+i);
        }

    }

    public void drawBoard(Graphics2D g2) {
        //Draw border
        g2.setStroke(new BasicStroke(4));
        Line2D.Double line1 = new Line2D.Double(95, 565, 230, 120);
        g2.draw(line1);

        Line2D.Double line2 = new Line2D.Double(230, 120, 600, 120);
        g2.draw(line2);

        Line2D.Double line3 = new Line2D.Double(600, 120, 520, 565);
        g2.draw(line3);

        Line2D.Double line4 = new Line2D.Double(95, 565, 520, 565);
        g2.draw(line4);
    }

    public static ArrayList<Peg> getPegs() {
        return pegs;
    }

    public static int getCurrentPlayer() {
        return currentPlayer;
    }

    public static int getWhoWon() {
        return whoWon;
    }
}
