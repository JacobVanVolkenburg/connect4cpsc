package unbcproject;

import java.awt.*;

/**
 * This file is part of a solution to
 * CPSC 110 Lab unbcproject Problem 1
 * <p>
 * [Desc]
 *
 * @author Jacob VanVolkenburg
 * Student Number: 230143007
 * @version 1
 */

public class Bead {
    private final int arrayX;
    private final int arrayY;
    private final int height;
    int whoPlaced = 0;
    private Color color;
    public Bead(int arrayX, int arrayY, int height, int whoPlaced){
        this.arrayX = arrayX;
        this.arrayY = arrayY;
        this.height = height;
        this.whoPlaced = whoPlaced;
        if(this.whoPlaced == 1){
            color = ColorPalette.player1;
        }else if(this.whoPlaced == 2){
            color = ColorPalette.player2;
        }
    }
    public Color getColor(){
        return color;
    }
    public void setColor(Color color){
        this.color = color;
    }

    /** Returns the bead next to it, or null if its at the edge
     *
     * @param dir
     * @return
     */
    public Bead getBeside(Direction dir){
        int newX = arrayX + dir.getX();
        int newY = arrayY + dir.getY();
        int newHeight = height + dir.getHeight();
        if(newX < 0 || newX >= 4){
            return null;
        }
        if(newY < 0 || newY >= 4){
            return null;
        }
        if(newHeight < 0 || newHeight >= 4){
            return null;
        }

        Peg adjacentPeg = GameFrame.pegArray[newX][newY];
        return adjacentPeg.getBead(newHeight);
    }
}
