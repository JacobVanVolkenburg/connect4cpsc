package unbcproject;


import java.awt.*;

/**
 * This file is part of a solution to
 * CPSC 110 Lab unbcproject Problem 1
 * <p>
 * [Desc]
 *
 * @author Jacob VanVolkenburg
 * Student Number: 230143007
 * @version 1
 */

public class Peg{

    private int arrayX;
    private int arrayY;
    private Rectangle rect;
    private Bead[] beads = new Bead[4];
    public Peg(int x, int y, int width, int height, int arrayX, int arrayY){
        this.arrayX = arrayX;
        this.arrayY = arrayY;
        rect = new Rectangle(x, y, width, height);
    }

    public void drawBeads(Graphics2D g2){
        for (int i = 0; i < beads.length; i++) {
            Bead b = beads[i];
            if (b != null) {
                g2.setColor(b.getColor());
                int radius = 20;
                g2.fillOval(this.getRectangle().x - (radius/2) + 2, 130 + this.getRectangle().y - (i * 25), radius, (int) (radius*1.5));

            }
        }
    }

    public Rectangle getRectangle(){
        return rect;
    }

    public int getArrayX() {
        return arrayX;
    }

    public int getArrayY() {
        return arrayY;
    }
    public Peg getLeft(){
        int newX = getArrayX() - 1;
        if(newX == -1){
            newX = 3;
        }
        return GameFrame.pegArray[newX][getArrayY()];
    }
    public Peg getRight(){
        int newX = getArrayX() + 1;
        if(newX == 4){
            newX = 0;
        }
        return GameFrame.pegArray[newX][getArrayY()];
    }
    public Peg getUp(){
        int newY = getArrayY() - 1;
        if(newY == -1){
            newY = 3;
        }
        return GameFrame.pegArray[getArrayX()][newY];
    }
    public Peg getDown(){
        int newY = getArrayY() + 1;
        if(newY == 4){
            newY = 0;
        }
        return GameFrame.pegArray[getArrayX()][newY];
    }

    public boolean dropBead() {
        if(GameComponent.whoWon != 0){
            return false;
        }
        for (int i = 0; i < 4; i++) {
            Bead bead1 = this.beads[i];
            if(bead1 == null){
                Bead bead = new Bead(getArrayX(), getArrayY(), i, GameComponent.getCurrentPlayer());
                this.beads[i] = bead;
                return true;
            }
        }
        System.out.println("already something here");
        return false;
    }

    public void clearBeads() {
        for (int i = 0; i < 4; i++) {
            beads[i] = null;
        }
    }
    public void removeBeadAt(int r) {
        beads[r] = null;
    }

    public Bead getBead(int i) {
        return beads[i];
    }

    @Override
    public String toString() {
        return "["+this.getArrayX() + ", " + this.getArrayY() + "]";
    }


}
