package unbcproject;

import javax.swing.*;
import java.util.ArrayList;

/**
 * This file is part of a solution to
 * CPSC 110 Lab unbcproject Problem 1
 * <p>
 * [Desc]
 *
 * @author Jacob VanVolkenburg
 * Student Number: 230143007
 * @version 1
 */

public class WinCheck {
    public static void checkWin(int whoPlaced){
        for (Peg p : GameComponent.getPegs()){
            for (int beadPlace = 0; beadPlace < 4; beadPlace++) {
                for (Direction dir : Direction.getAllDirections()){
                    if(has4InARow(p, beadPlace, dir)){
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!WIN!!!!!!!!!!!!!!!!!!!!!!!!");
                        Bead startingBead = p.getBead(beadPlace);
                        Bead endBead = startingBead;
                        //Move 3 times in direction of win
                        for (int i = 0; i < 3; i++) {
                            endBead = startingBead.getBeside(dir);
                        }

                        GameComponent.playerWin(whoPlaced);
                        return;
                        //UserInput.resetGame();
                    }else{
                        //no win for this specific row, continue on to the next
                    }
                }
            }
        }
        System.out.println("NO WIN");
        //has4InARow(GameFrame.pegArray[0][0], 0, new Direction(0,1,0));
    }
    private static boolean has4InARow(Peg peg, int beadPlace, Direction dir){
        Bead currentBead = peg.getBead(beadPlace);
        if(currentBead == null){
            return false;
            //Return false if there is no bead
        }

        //Save checked beads in a list incase they are the winning beads
        ArrayList<Bead> winningBeads = new ArrayList<>();
        winningBeads.add(currentBead);


        //Check if the next 3 beads in direction 'dir' are placed by the same person

        for (int i = 0; i < 3; i++) {
            Bead beside = currentBead.getBeside(dir);
            if(beside == null){
                return false;
            }
            if(currentBead.whoPlaced == beside.whoPlaced){
                //good
                currentBead = beside;
                winningBeads.add(currentBead);
            } else {
                debug("Bead is not the same");
                return false;
            }
        }
        GameComponent.markBeadsAsGreen(winningBeads);
        return true;
    }
    private static void debug(String s){
        System.out.println("[Debug] " + s);
    }
}
