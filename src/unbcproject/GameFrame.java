package unbcproject;
import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

/**
 * <p>
 * [Desc]
 *
 * @author Jacob VanVolkenburg
 * Student Number: 230143007
 * @version 1
 */

public class GameFrame extends JFrame {

    public static GameComponent component;
    public static GameFrame frame;

    public GameFrame(){
        frame = this;
        this.setResizable(true);
        this.setSize(1000,800);
        this.setTitle("Connect 4 Game");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        component = new GameComponent();

        this.addKeyListener(new KeyboardListener());
        //this.add(component);
        //JPanel panel = new JPanel();
        //panel.setBounds(0,0,800,800);
        //panel.add(component);
        this.add(component);
        //panel.setVisible(true);
        this.setVisible(true);
        repaint();
    }



   public static  Peg[][] pegArray = new Peg[4][4];
    public static boolean player1AI;
    public static boolean player2AI;
    public static void startFrame(boolean player1AI, boolean player2AI, boolean startAsYellow) {
        GameFrame.player1AI = player1AI;
        GameFrame.player2AI = player2AI;
        GameFrame game = new GameFrame();
        int apart = 100;
        int startingPointX = 250;
        int startingPointY = 100;

        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                Peg peg = new Peg((startingPointX + x*apart ) - y*30, startingPointY + y*apart , 5, 150, x, y);
                GameComponent.getPegs().add( peg);
                GameComponent.getPlayer().selected = peg;
                GameFrame.pegArray[x][y] = peg;
                //
            }
        }
        if(startAsYellow){
            GameComponent.setCurrentPlayer(2);
        }
        if(player1AI || player2AI){
            GameComponent.switchPlayer();
        }
    }
}
